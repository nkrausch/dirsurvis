#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Niels Krausch
@version: 0.2

Visualization of the DirSurveiller

This tool creates a visualisation of how a specific file was created via a specific pipeline of other tools.
"""

import argparse
import ast
import json
import os
import re
import signal
import subprocess
import sys
import threading
import webbrowser
from itertools import chain
from time import sleep


class Data:
    """Class for obtaining und storing Open- and ExecCalls"""

    def __init__(self, filename):
        """Initialisation of the data class

        Args:
            filename (str): The root file

        Attributes:
                self.open_calls (dict): All openCalls from DirSurveiller
                self.exec_calls (dict): All execCalls from DirSurveiller
                self.__filename (str): The root file
        """

        self.__open_calls = {}
        self.__exec_calls = {}
        self.__read_files_cmds = []
        self.__written_files_cmds = []
        self.__filename = filename

    def get_data(self):
        """Obtain exec- and open Calls.

        Get exec and open Calls from DirSurveiller, search for filename (openCall) or program name (execCall) and store
        them as attributes."""

        # Set absolute path, necessary for proper working of the DirSurveiller
        if not os.path.isabs(self.__filename):
            path = os.path.join(os.getcwd(), self.__filename)
        else:
            path = self.__filename

        # Start DirSurveiller as subprocess
        with subprocess.Popen(["watchdog_ctrl -gformat=json {}".format(path)], shell=True,
                              stdout=subprocess.PIPE) as process:

            # Convert output stream to string and store in variable
            out = process.stdout.read().decode("utf-8")

        # Save multiple JSON formatted openCalls as list. ast provides safety eval()
        self.__open_calls = [ast.literal_eval(entry) for entry in re.findall(r"\"openCall\"\s?=\s?({.*})}", out)]

        # Save multiple JSON formatted execCalls as list. ast provides safety eval()
        self.__exec_calls = [ast.literal_eval(entry) for entry in re.findall(r"\"execCalls\"\s?=\s?\[?({.*})",
                                                                             out)]

        # Store all read files and associated cmds
        self.__read_files_cmds = re.findall(r"\"flag\":\s?0,\s?\"filepath\":\s?\"(?:.*/)(.*)\".*\n?\r?\s?{\"execCalls\""
                                            r".*?\"cmd\":\s?\"(.*?)\",.*(?:\"cmd\":\s?\".*\",.*)*", out)

        # Store all written files and associated cmds
        self.__written_files_cmds = re.findall(r"\"flag\":\s?1,\s?\"filepath\":\s?\"(?:.*/)(.*)\".*\n?\r?\s?{"
                                               r"\"execCalls\".*?\"cmd\":\s?\"(.*?)\",.*(?:\"cmd\":\s?\".*\",.*)*", out)

    def get_open_calls(self):
        """Returns all used files.

        Returns:
            list: All openCalls"""

        return [file for file in self.__open_calls]

    def get_exec_calls(self):
        """Returns all execCalls.

        Returns:
            list: All execCalls"""

        return [call for call in self.__exec_calls]

    def get_read_files_cmds(self):
        """Returns all read files and the associated cmds, which opened them.

        Returns:
            list: All read files and associated cmds"""

        return [file_cmd for file_cmd in self.__read_files_cmds]

    def get_written_files_cmds(self):
        """Returns all written files and the associated cmds, which created them.

        Returns:
            list: All written files and associated cmds"""

        return [file_cmd for file_cmd in self.__written_files_cmds]

    @staticmethod
    def get_filename(open_call):
        """Returns filename from openCall.

        Args:
            open_call (str): The actual opencall from DirSuveiller containing the path

        Returns:
            str: Just the program name without path"""

        return re.findall(r"(?:.*/)(.*)\"", open_call)

    @staticmethod
    def get_cmds(exec_calls):
        """Returns parent and child processes.

        Args:
            exec_calls (dict): The execCall from DirSurveiller

        Returns:
            parent (str): The parent of the process
            children (list): The child process"""
        parent = ""
        children = []
        for call in exec_calls:
            if call["pid"] == call["ppid"]:
                parent = call["cmd"]
            else:
                children.append(call["cmd"])
        return parent, children


class Tree:
    """Base class for nodes and edges"""

    def __init__(self, filename):
        """Initialization of the graph.

        Args:
            filename (str): The root file

        Attributes:
            self.root_path (str): The root file containing its path
            self.root_file (str): The root file without path
            self.__tree_nodes (list): The Nodes (files) of the tree
            self.__tree_edges (dict): The edges (cmds with args) of the tree"""

        self.__root_path = filename
        self.__root_file = os.path.basename(filename)
        self.__tree_nodes = []
        self.__tree_edges = {}

    def get_nodes(self):
        """Return the nodes of the graph"""
        return self.__tree_nodes

    def get_edges(self):
        """Return the edges of the graph"""
        return self.__tree_edges

    def get_children(self, node):
        """Return the children of a respective node.

        Args:
            node (str): A particular node in the tree

        Returns:
            list: The children of the node."""

        try:
            return self.get_edges()[node]["children"]
        except KeyError:
            pass

    def add_node(self, node):
        """Add node if not present in current tree.

        Args:
            node (str): A particular node"""

        if node not in self.__tree_nodes:
            self.__tree_nodes.append(node)

    def add_edge(self, node1, node2, cmd):
        """Add edge to current tree.

        Args:
            node1 (str): Input file for the connecting command
            node2 (str): Output file for the connecting command
            cmd (str): The connecting command"""

        if node1 in self.__tree_nodes and node2 in self.__tree_nodes:
            if node1 not in self.__tree_edges:
                self.__tree_edges[node1] = {"children": [{"file": node2, "cmd": cmd}]}
            else:
                self.__tree_edges[node1]["children"].append({"file": node2, "cmd": cmd})
        else:
            raise KeyError("Node not found in tree")

    def add_data(self):
        """Add data to tree.

        Obtains all open- and execCalls from the Dirsurveiller, adds the files as nodes to the tree, searches for
        links between the nodes by looking up which program read or wrote a file respectively and adds these links
        to the tree. Plus it deletes files, which are not useful (i.e. discarded sequences) from the tree."""

        # Initialize data instance
        data = Data(self.__root_path)

        # Obtain data from DirSurveiller
        data.get_data()

        # Add filenames as nodes to tree
        for file in data.get_open_calls():
            self.add_node(os.path.basename(file["filepath"]))

        # Store all written files and associated cmds in dict
        written_files = dict(data.get_written_files_cmds())

        # Store all read files and associated cmds in (inverted) dict
        read_files = dict()
        for file, cmd in data.get_read_files_cmds():
            if cmd in read_files:
                read_files[cmd].append(file)
            else:
                read_files[cmd] = [file]

        # Check if file is orphaned: therefore created, but never read again (e.g. out_discarded.fastq)
        for file in self.__tree_nodes:
            if file not in list(chain.from_iterable(read_files.values())) and file != self.__root_file:
                self.__tree_nodes.remove(file)

        # Add edges between nodes
        for file in self.__tree_nodes:
            try:
                for child_node in read_files[written_files[file]]:
                    self.add_edge(file, child_node, written_files[file])
            except KeyError:
                continue

    def generate_tree(self, node=None, cmd="null"):
        """Generate the actual tree.

        Generates the tree based on nodes and edges downstream from the referenced node. This function is called
        recursive until no node is left.

        Args:
            node (str): Current node to build tree from. Defaults to None -> Root Node.
            cmd (str): Command which belongs to the current node (file). Defaults to "null".

        Returns:
            tree (dict): The complete nested tree of all nodes and edges"""

        # Use user input as tree root
        if node is None:
            node = self.__root_file
        tree = {"file": node, "cmd": cmd}
        children = self.get_children(node)
        if children:
            tree["children"] = [self.generate_tree(child["file"], child["cmd"]) for child in children]
        return tree

    def generate_json(self):
        """Generate a nested json of the tree.

        Generates the actual tree, converts the data to JSON and stores them in a file, which will be parsed by D3"""

        tree = self.generate_tree(self.__root_file)

        # Store tree in JSON formated file
        with open('./static/flare.json', 'w') as f:
            json.dump(tree, f, indent=4)


class FlaskApp(threading.Thread):
    """Simple thread class for the flask app.

    Attributes:
        self.threadID (int): The ID of the thread."""

    def __init__(self, thread_id):
        """Initialization of the flask app class"""
        threading.Thread.__init__(self)
        self.threadID = thread_id

    def run(self):
        """Start the thread"""
        print(" * Starting visualizer as thread no. " + str(self.threadID))
        os.system("flask run")


def signal_handler(signum, frame):
    """Simple handler for catching SIGINT signal.

    Args:
        signum (Signals): The signal received from the OS.
        frame (FrameObject): Stack frame from the point in the program that was interrupted by the signal."""

    if signum != 2 and signum != 15:
        print("Received signal " + signum + " at " + frame)

    # Display shutdown message
    print("Shutting down")

    # Terminate program
    sys.exit(0)


def main():
    """Main function.

    This function performs the actual tasks to build the tree from the DirSurveiller output"""

    # Parse arguments
    parser = argparse.ArgumentParser(description="Generate graphical tree from DirSurveiller")
    parser.add_argument("filename")
    args = parser.parse_args()

    # Signal handler for terminating the program
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)

    # Create flare.json
    tree = Tree(args.filename)
    tree.add_data()
    tree.generate_json()

    # Set Path variable
    os.environ["FLASK_APP"] = "DirSurVis.py"

    try:
        # Start flask app
        thread = FlaskApp(1)
        thread.start()

        # Wait for flask app to start and open new browser tab with graph
        sleep(0.5)
        webbrowser.open('http://localhost:5000/', new=2)
        signal.pause()
    except RuntimeError:
        print("Error: Unable to start thread")


if __name__ == "__main__":
    main()
