#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Niels Krausch
@version: 0.2

Visualization of the DirSurveiller

This script provides the flask app to show the respective graph.
"""
from flask import Flask, request, url_for, render_template


app = Flask(__name__)  # create the application instance


@app.route('/')
def show_entries():
    """Shows the graph file"""
    return render_template('layout.html')


def shutdown_server():
    """Shuts down the server"""
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()


@app.route('/shutdown', methods=['POST'])
def shutdown():
    shutdown_server()
    return 'Server shutting down...'
