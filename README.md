# DirSurveillerVisualizer
This tool analyzes and visualizes the output of DirSurveiller and creates a collapsible tree diagram with D3 to
reproduce how a particular input file was generated.

## Prerequisites
This program was implemented in Python and requires Python 3.4 or higher.<br/>

Required software or modules:
* flask
* [DirSurveiller](https://github.com/rekm/DirSurveiller)

Please install these packages either with pip, conda or compile from source.

First compile DirSurveiller and add the folder(s) you want to surveil. See the DirSurveiller Readme for further
instructions.

## Usage
After setting up the DirSurveiller and performing your analysis you can call the DirSurVis on a particular file.

```
    python createGraph.py /path/to/file.ext
```

It may happen sometimes, that your browser caches an old version of the graph, please reload the page if this happens.

A sample call of this program:
```
    python createGraph.py /home/test/output/mapped.bam
```

## Authors
* **Niels Krausch**

## License
This project is licensed under the AGPL v3.0 License - see the [LICENSE](LICENSE) file for details.

## Acknowledgments
* Thanks to René Kmiecinski for providing the DirSurveiller
* Developed with resources thankfully provided by the Robert Koch-Institute Berlin